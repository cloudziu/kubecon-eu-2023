package main

import (
	"fmt"
	"strconv"
	"strings"
	"testing"
	"time"
)

func TestGetAsciiImage(t *testing.T) {

	fmt.Println("Testing GetAsciiImage")

	// Fake the image string (loading and converting an image fails for some reason in the test scope)
	res := GetAsciiImage("2023", helloFrom)

	// Check if the current year is included the image intro string
	tables := []string{strconv.Itoa(time.Now().Year())}

	for _, table := range tables {
		fmt.Printf("Checking GetAsciiImage for '%s'.", table)
		if strings.Contains(res, table) != true {
			t.Errorf("GetAsciiImage failed at checking for '%s'.", table)
		}
	}
}
